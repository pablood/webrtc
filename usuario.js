import DB from './database';

class Usuario {

    constructor(){
        this.id = null;
        this.nombre_usuario = null;
        this.nombre_completo = null;
    }

    save(){
        let self = this;
        return new Promise((fulfill, reject) => {

            DB.query({
                sql: 'select * from usuarios where id_externo = ?',
                values: [self.id_externo]
            }, function (error, results) {
                if(error) reject(error);
                if(results.length){
                    self.id =  results[0].id;
                    fulfill(self)
                }else{
                    DB.query({
                        sql: 'insert into usuarios (nombre_usuario, nombre_completo, id_externo) values(?,?, ?)',
                        values: [self.nombre_usuario, self.nombre_completo, self.id_externo]
                    }, (error, results) => {
                        if(error) reject(error);
                        else{
                            self.id =  results.insertId;
                            fulfill(self);
                        }
                    });
                }
            });
        });
    }

    static AllInSession(sesionId){
        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'select id_usuario, nombre_usuario, nombre_completo, rol, id_externo, min(fecha_conexion), max(fecha_desconexion) from sesiones_usuarios\n' +
                'left join usuarios on sesiones_usuarios.id_usuario = usuarios.id\n' +
                'where sesiones_usuarios.id_sesion = ?\n' +
                'group by id_usuario,rol',
                values:[sesionId]
            }, function (error, results) {
                if(error) reject(error);
                fulfill(results);
            })
        })
    }


}

export default Usuario;