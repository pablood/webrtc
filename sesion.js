import DB from './database';

class Sesion {

    constructor(){
        this.curso = '';
        this.modo_actual = '';
        this.contenido_editor = '';
        this.fecha_inicio = null;
        this.fecha_fin = null;
        this.usuario_activo = null;
        this.usuario_moderador = null;
        this.usuarios = [];
    }

    estaVacia(){
        return Object.keys(this.usuarios).length === 0;
    }

    numeroParticipantes(){
        return Object.keys(this.usuarios).length;
    }

    agregarUsuario(usuario, esModerador){

        let self = this;

        return new Promise((fulfill, reject) => {

            DB.query({
                sql: 'insert into sesiones_usuarios (id_sesion, id_usuario, pide_palabra) values(?,?,?)',
                values: [self.id, usuario.id, false]
            }, (error) => {
                if(error) reject (error);
                else {
                    // Caso 1: la sesion esta vacia: el primer usuario se convierte en tutor
                    if(self.estaVacia()){
                        self.usuario_activo = usuario;
                        self.usuario_moderador = usuario;
                        self.usuarios[usuario.nombre_usuario] = usuario;
                        self.update()
                            .then(() => fulfill())
                            .catch((error) => reject(error));
                    }
                    // Caso 2: la sesion no esta vacia, si el nuevo usuario es tutor, le quita el mando al usuari actual
                    else if((self.usuario_activo === null || self.usuario_activo.rol !== 'tutor')  && esModerador ){
                        self.usuario_activo = usuario;
                        self.usuario_moderador = usuario;
                        self.usuarios[usuario.nombre_usuario] = usuario;
                        self.update()
                            .then(() => fulfill())
                            .catch((error) => reject(error));
                    }
                    // Caso 3: la sesion no esta vacia, ni el participante es tutor
                    else {
                        self.usuarios[usuario.nombre_usuario] = usuario;
                        fulfill();
                    }
                }
            });
        });

    }

    quitarUsuario(usuario){

        let self = this;

        return new Promise((fulfill, reject) => {

            DB.query({
                sql: 'update sesiones_usuarios set fecha_desconexion = ? where id_sesion = ? and id_usuario = ?',
                values: [Sesion.CurrentTimestamp(), self.id, usuario.id]
            }, (error) => {
                if (error) reject(error);
                else {

                    // Se saca el usuario del objeto sesion
                    delete this.usuarios[usuario.nombre_usuario];

                    // Caso 1: se desconecta un usuario que no tiene la palabra ni es tutor
                    // No se realiza ninguna accion especial

                    // Caso 2: se desconecta un usuario que tiene la plabra pero no es tutor
                    // La palabra vuelve al tutor
                    let caso2 = (usuario.id === self.usuario_activo.id && usuario.id !== self.usuario_moderador.id);
                    if(caso2){
                        self.usuario_activo = self.usuario_moderador;
                    }

                    // Caso 3: se desconecta el tutor, pero no no tenia la palabra
                    // El usuario activo se convierte en tutor
                    let caso3 = (usuario.id === self.usuario_moderador.id && usuario.id !== self.usuario_activo.id);
                    if(caso3){
                        self.usuario_moderador = self.usuario_activo;
                    }

                    // Caso 4: se desconecta el tutor, que si tiene la palabra
                    let caso4 = (usuario.id === self.usuario_moderador.id && usuario.id === self.usuario_activo.id);
                    if(caso4){

                        // Dos posibles casos

                        // Caso 4A: No hay mas participantes
                        // Se cierra la sesion
                        let caso4A = self.estaVacia();
                        if(caso4A) {
                            self.fecha_fin = Sesion.CurrentTimestamp();
                        }
                        // Caso 4B: Aun hay participantes
                        // Elegimos un reemplazo al azar
                        else{
                            let keys = Object.keys(this.usuarios);
                            self.usuario_activo = this.usuarios[keys[Math.floor(keys.length * Math.random())]];
                            self.usuario_moderador = self.usuario_activo;
                        }
                    }

                    if(caso2 || caso3 || caso4){
                        self.update()
                            .then(() => fulfill())
                            .catch((error) => reject(error));
                    }else{
                        fulfill();
                    }
                }
            })
        })

    }

    expulsarUsuario(nombreUsuarioExpulsado){
        let self = this;
        return new Promise((fulfill, reject) => {

            let usuarioExpulsado = self.usuarios[nombreUsuarioExpulsado];

            DB.query({
                sql: 'update sesiones_usuarios set fecha_desconexion = ? where id_sesion = ? and id_usuario = ?;' +
                'insert into sesiones_interacciones (id_sesion, id_usuario, tipo_interaccion, valor) values (?,?,?,?);',
                values: [Sesion.CurrentTimestamp(), self.id,  usuarioExpulsado.id,
                    self.id, self.usuario_activo.id, 'expulsion', usuarioExpulsado.id
                ]
            }, (error) => {
                if (error) reject(error);
                else {

                    delete this.usuarios[nombreUsuarioExpulsado];

                    if(self.estaVacia()){
                        self.fecha_fin = Sesion.CurrentTimestamp();
                        self.update()
                            .then(() => fulfill())
                            .catch((error) => reject(error));
                    }else{
                        fulfill();
                    }
                }
            })
        })
    }

    cambiarUsuarioActivo(nombreUsuarioDesde, nombreUsuarioHacia){
        let self = this;
        let usuarioDesde = self.usuarios[nombreUsuarioDesde];
        let usuarioHacia = self.usuarios[nombreUsuarioHacia];
        return new Promise((fulfill, reject) => {

            self.usuario_activo = usuarioHacia;
            self.update()
                .then(() => {
                    let tipo_interaccion = (nombreUsuarioDesde === nombreUsuarioHacia) ? 'recuperar control' : 'otorgar control';
                    DB.query({
                        sql: 'insert into sesiones_interacciones (id_sesion, id_usuario, tipo_interaccion, valor) values (?,?,?,?);'+
                        'update sesiones_usuarios set pide_palabra = 0 where id_sesion = ? and id_usuario = ?;',
                        values: [self.id, usuarioDesde.id, tipo_interaccion, usuarioHacia.id, self.id, usuarioHacia.id]
                    }, (error) => {
                        if(error) reject(error);
                        else fulfill();
                    });

                })
                .catch((error) => {
                    reject(error);
                })
        });

    }

    cambiarModo(modo, usuario){
        let self = this;
        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'update sesiones sesiones set modo_actual = ? where id = ?;' +
                'insert into sesiones_interacciones (id_sesion, id_usuario, tipo_interaccion, valor) values (?,?,?,?)',
                values: [
                    modo, self.id,
                    self.id, usuario.id, 'cambio modo', modo]
            }, function(error){
                if (error) reject(error);
                else fulfill();
            });
        });

    }


    enviarMensajeGeneral(nombreUsuario, mensaje){

        let self = this;
        let usuario = self.usuarios[nombreUsuario];

        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'insert into chats_generales (id_sesion, id_usuario, mensaje) values(?,?,?)',
                values: [self.id, usuario.id, mensaje]
            },function (error) {
                if(error) reject(error);
                else fulfill();
            });
        });

    }

    enviarMensajeUsuario(nombreUsuarioDesde, nombreUsuarioHacia, mensaje){

        let self = this;
        let usuarioDesde = self.usuarios[nombreUsuarioDesde];
        let usuarioHacia = self.usuarios[nombreUsuarioHacia];

        return new Promise((fulfill, reject) => {

            DB.query({
                sql: 'insert into chats_individuales (id_sesion, id_usuario_emisor, id_usuario_receptor, mensaje) values (?,?,?,?)',
                values: [self.id, usuarioDesde.id, usuarioHacia.id, mensaje]
            },function (error) {
                if(error) reject(error);
                else fulfill();
            });
        });
    }

    chatsGenerales(){

        let self = this;

        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'select nombre_usuario as usuario, mensaje\n' +
                'from chats_generales\n' +
                'inner join usuarios on usuarios.id = chats_generales.id_usuario\n' +
                'where id_sesion = ? ',
                values: [self.id]
            }, (error, results) => {
                if(error) reject(error);
                else fulfill(results);
            });
        })
    }



    update(){
        let self = this;
        let usuarioActivo = (self.usuario_activo) ? self.usuario_activo.id : null;
        let usuarioModerador = (self.usuario_moderador) ? self.usuario_moderador.id : null;
        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'update sesiones set usuario_activo = ?, usuario_moderador = ?, fecha_fin = ? where id = ?',
                values: [usuarioActivo, usuarioModerador, self.fecha_fin, self.id],
            },(error) => {
                if(error) reject(error);
                else{
                    fulfill();
                }
            });
        });
    }

    save(){
        let self = this;
        return new Promise((fulfill, reject) => {

            DB.query({
                sql: 'insert into sesiones (curso, modo_actual) values(?,?)',
                values: [self.curso, self.modo_actual]
            }, (error, results) => {
                if(error) reject(error);
                else{
                    self.id = results.insertId;
                    fulfill(self)
                }
            })
        })
    }

    static CurrentTimestamp(){
        return (new Date()).toISOString().substring(0, 19).replace('T', ' ')
    }

    // Funciones estaticas para el web service

    static All(){
        return new Promise((fulfill,reject)=>{
            DB.query({
                sql: 'select * from sesiones'
            },(error, results) => {
                if(error) reject(error);
                else fulfill(results);
            })
        })
    }

    static ChatGeneral(sesionId){
        return new Promise((fulfill, reject) => {
           DB.query({
               sql: 'select id_externo, nombre_completo, mensaje, fecha from chats_generales\n' +
               'inner join usuarios on usuarios.id = chats_generales.id_usuario\n' +
               'where id_sesion = ?\n' +
               'order by fecha asc',
               values: [sesionId]
           }, (error, results) => {
               if(error) reject(error);
               fulfill(results);
           })
        });
    }

    static ChatIndividuales(sesionId){
        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'select u1.id_externo as id_emisor, u1.nombre_completo as nombre_emisor, u2.id_externo as id_receptor, u2.nombre_completo as nombre_receptor, mensaje, fecha \n' +
                'from chats_individuales\n' +
                'inner join usuarios u1 on  id_usuario_emisor = u1.id \n' +
                'inner join usuarios u2 on  id_usuario_receptor = u2.id\n' +
                'where id_sesion = ?\n' +
                'order by fecha asc',
                values: [sesionId]
            }, (error, results) => {
                if(error) reject(error);
                fulfill(results);
            })
        });
    }

    static Interacciones(sesionId, tipo){

        let query = {sql: '', values: []};
        switch(tipo){
            case 'expulsion':
            case 'otorgar control':
            case 'recuperar control':
                query = {sql: 'select u1.id_externo as id_tutor, u1.nombre_completo as nombre_tutor, u2.id_externo as id_alumno, u2.nombre_completo as nombre_alumno, fecha\n' +
                'from sesiones_interacciones \n' +
                'join usuarios u1 on u1.id = id_usuario\n' +
                'join usuarios u2 on u2.id = valor\n' +
                'where id_sesion = ? and tipo_interaccion = ?\n' +
                'order by fecha asc', values:[sesionId, tipo]};
                break;
            case 'pedir control':
                query = {sql: 'select u.id_externo, u.nombre_completo, fecha\n' +
                'from sesiones_interacciones \n' +
                'join usuarios u on u.id = id_usuario\n' +
                'where id_sesion = ? and tipo_interaccion = ?\n' +
                'order by fecha asc', values:[sesionId, tipo]};
                break;
            case 'cambio modo':
                query = {sql: 'select u.id_externo, u.nombre_completo, valor, fecha\n' +
                'from sesiones_interacciones \n' +
                'join usuarios u on u.id = id_usuario\n' +
                'where id_sesion = ? and tipo_interaccion = ? \n' +
                'order by fecha asc', values:[sesionId, tipo]};
                break;
        }

        return new Promise((fulfill, reject) => {

            DB.query(query, (error, results) => {
                if(error) reject(error);
                fulfill(results);
            })
        });
    }

    static Delete(sessionId){
        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'delete from sesiones where id = ?',
                values: [sessionId]
            }, (error, results) => {
                if(error) reject(error);
                fulfill(results);
            })
        });
    }
}

export default Sesion;