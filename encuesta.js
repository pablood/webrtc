import DB from './database';

class Encuesta {

    constructor() {
        this.pregunta = '';
        this.sesion = null;
        this.usuario = null;
    }

    crear(){

        let self = this;

        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'insert into encuestas (id_sesion, id_usuario, pregunta) values (?,?,?)',
                values: [self.sesion.id, self.usuario.id, self.pregunta]
            }, function (error, results) {
                if(error) reject(error);
                else{
                    self.id = results.insertId;
                    fulfill(self);
                }

            });
        });

    }


    static AllInSession(sesionId){
        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'select encuestas.id as id_encuesta, pregunta, nombre_usuario, nombre_completo, id_externo, fecha from encuestas \n' +
                'inner join usuarios on encuestas.id_usuario = usuarios.id\n' +
                'where encuestas.id_sesion = ?',
                values: [sesionId]
            }, function (error, results) {
                if(error) reject(error);
                fulfill(results);
            });
        });
    }

    static AllRespuestas(encuestaId){
        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'select id_externo, nombre_completo, case when respuesta is not null then \'true\' else \'false\' end respuesta, fecha\n' +
                'from encuestas_respuestas\n' +
                'inner join usuarios on usuarios.id = encuestas_respuestas.id_usuario\n' +
                'where id_encuesta = ?',
                values: [encuestaId]
            }, function (error, results) {
                if(error) reject(error);
                fulfill(results);
            });
        });
    }

}

export default Encuesta;