import _ from 'lodash';
import DB from './database';
import Encuesta from './encuesta';
import Sesion from './sesion';
import Usuario from './usuario';

class InfoMeeting {

    constructor(){

        /** Estructura del objeto salas

         salas:
            matematica:
                sesion: {obj sesion}
                conexiones:
                    [user1: wsConnection]
                        curso: str
                        usuario: {obj usuario}
                        idsesion: idsesion
                        requestedControl: bool
                        ...
                    [user2: wsConnection]
                        curso: str
                        usuario: {obj usuario}
                        idsesion: idsesion
                        requestedControl: bool
                        ...
            programacion:
                sesion
                conexiones []

         **/
        this.salas = [];

    }

    /** Flujo basico de conexion / desconexion **/

    handleLogin(connection, data){

        DB.query({
            sql: 'select * from tokens where token = ? order by id desc limit 1',
            values: [data.token]
        }, (error, results) => {

            if(error){
                InfoMeeting.SendTo(connection, {
                    type: "login",
                    success: false,
                    error: 'No se pudo conectar a la base de datos, Reinicie el servidor'
                });
                return false;
            }

            if(results.length === 0) {
                InfoMeeting.SendTo(connection, {
                    type: "login",
                    success: false,
                    error: 'Token invalido'
                });
            }

            try{
                let curso = results[0].curso;
                let nombreUsuario = results[0].nombre_usuario;
                let nombreCompleto = results[0].nombre_completo;
                let idExterno = results[0].id_externo;
                let rol = results[0].rol;

                let sesion  = (this.salas[curso] === undefined) ? undefined : this.salas[curso].sesion;

                let self = this;
                let esElPrimerParticipanteDelCurso = (sesion === undefined || sesion.estaVacia());
                if(esElPrimerParticipanteDelCurso){
                    sesion = new Sesion();
                    sesion.curso = curso;
                    sesion.modo_actual = 'video';
                    sesion.save()
                        .then((sesion) => {
                            self.salas[curso] = {};
                            self.salas[curso].sesion = sesion;
                            self.salas[curso]['conexiones'] = {};
                        })
                        .catch((error) => {
                            console.log(error);
                        });

                }

                let addUserToSession = ()=> {
                    let usuario = new Usuario();
                    usuario.nombre_usuario = nombreUsuario;
                    usuario.nombre_completo = nombreCompleto;
                    usuario.id_externo = idExterno;
                    usuario.rol = rol;
                    usuario.save()
                        .then((objUsuario) => {
                            usuario = objUsuario;
                            return sesion.agregarUsuario(usuario, rol === 'tutor');
                        })
                        .then(() => {

                            // agregar conexion
                            connection.curso = curso;
                            connection.usuario = usuario;
                            connection.idSesion = sesion.id;

                            console.log('agrega a ', usuario.nombre_usuario)
                            this.salas[curso]['conexiones'][usuario.nombre_usuario] = connection;

                            this.currentState(sesion.id, usuario.id)
                                .then((state) => {
                                    InfoMeeting.SendTo(connection, {
                                        type: "login",
                                        success: true,
                                        connectedUser: usuario,
                                        state: state
                                    });

                                    // notifica a los participantes de la sesion que hay un nuevo usuario
                                    _.forIn(self.salas[curso]['conexiones'], function (connection) {
                                        if (connection.usuario !== usuario) {
                                            InfoMeeting.SendTo(connection, {
                                                type: "newuser",
                                                connectedUser: usuario,
                                                state: state
                                            });
                                        }
                                    });

                                })
                                .catch((error) => console.log(error));

                            sesion.chatsGenerales()
                                .then((lines) => {
                                    InfoMeeting.SendTo(connection, {
                                        type: "chat",
                                        chat: 'main',
                                        initial: true,
                                        lines: lines
                                    });
                                })
                                .catch((error) => console.log(error))
                        })
                        .catch((error) => {
                            console.log(error);
                            InfoMeeting.SendTo(connection, {
                                type: "login",
                                success: false,
                                error: error
                            });
                        });
                }

                // verificamos que no haya un usuario con el mismo username
                if(this.salas[curso] && this.salas[curso]['conexiones'][nombreUsuario]){
                    this.closeConnection(this.salas[curso]['conexiones'][nombreUsuario])
                        .then(() => {setTimeout(addUserToSession, 8000)});
                }else{
                    addUserToSession();
                }

            }
            catch (error){
                console.log(results);
                console.log(error);
            }
        });

    }

    handleOffer(connection, data){
        InfoMeeting.SendTo(this.salas[connection.curso]['conexiones'][data.to], {
            type: "offer",
            offer: data.offer,
            name: data.from.nombre_usuario
        });
    }

    handleAnswer(connection, data){
        InfoMeeting.SendTo(this.salas[connection.curso]['conexiones'][data.toName], {
            type: "answer",
            answer: data.answer,
            name: data.from.nombre_usuario
        });
    }

    handleCandidate(connection, data){
        _.forIn(this.salas[connection.curso]['conexiones'], (connection, username) => {
            if(username !== data.fromName){
                InfoMeeting.SendTo(connection, {
                    type: "candidate",
                    candidate: data.candidate,
                    name: data.fromName
                });
            }
        });
    }

    closeConnection(connection){

        return new Promise((fulfill, reject) => {
            let curso = connection.curso;
            let usuario = connection.usuario;
            let self = this;

            if (curso && usuario) {
                let sesion = this.salas[curso].sesion;

                sesion.quitarUsuario(usuario)
                    .then(() => {

                        self.currentState(connection.idSesion, usuario.id)
                            .then((state) => {
                                _.forIn(self.salas[curso]['conexiones'], (connection) => {
                                    console.log('manda el leave a ', connection.usuario.nombre_usuario)
                                    InfoMeeting.SendTo(connection, {
                                        type: "leave",
                                        state: state,
                                        disconnectedUser: usuario
                                    });
                                });
                                console.log('borra a ', usuario.nombre_usuario);
                                delete self.salas[curso]['conexiones'][usuario.nombre_usuario];
                                fulfill()
                            })
                    })
                    .catch((error) => {
                        console.log(error);
                        reject();
                    });
            }else{
                reject();
            }
        })
    }


    /** Cambio de control **/

    handleRequestControl(connection, data){

        let self = this;
        let curso = connection.curso;
        let usuario = connection.usuario;
        let sesion = self.salas[curso].sesion;
        let usuarioQuePideControl = data.user;

        DB.query({
            sql: 'insert into sesiones_interacciones (id_sesion, id_usuario, tipo_interaccion, valor) values (?,?,?,?);'+
            'update sesiones_usuarios set pide_palabra = 1 where id_sesion = ? and id_usuario = ?',
            values: [connection.idSesion, usuarioQuePideControl.id, 'pedir control', '',
                connection.idSesion, usuarioQuePideControl.id]
        }, (error) => {
            if(error) console.log(error);
            else{
                this.currentState(connection.idSesion, usuario.id)
                    .then((currentState) => {
                        self.salas[curso]['conexiones'][usuarioQuePideControl.nombre_usuario].requestedControl = true;
                        InfoMeeting.SendTo(self.salas[curso]['conexiones'][sesion.usuario_moderador.nombre_usuario], {
                            type: 'requestControl',
                            state: currentState
                        });
                    })
                    .catch((error) => console.log(error));
            }

        });

    }

    handleChangeActiveUser(connection, data){

        let self = this;
        let curso = connection.curso;
        let sesion = self.salas[curso].sesion;

        /*
        let textoCambioDuranteCambioDeControl = self.salas[curso].sesiontext != this.lastText;
        if(textoCambioDuranteCambioDeControl){
            sql += 'insert into sesiones_interacciones (id_sesion, id_usuario, tipo_interaccion, valor, fecha) values (?,?,?,?,?)';
        }
        */

        sesion.cambiarUsuarioActivo(data.usuarioDesde.nombre_usuario, data.usuarioHacia.nombre_usuario)
            .then(() => {
                self.currentState(sesion.id, connection.usuario.id)
                    .then((state) => {
                        _.forIn(self.salas[curso]['conexiones'], function (connection) {
                            InfoMeeting.SendTo(connection, {
                                type: "changeActiveUser",
                                state: state,
                            });
                        });
                    })
                    .catch((error) => console.log(error))
            })
            .catch((error) => console.log(error));

    }

    handleExpulsarUsuario(connection, data){

        let self = this;
        let curso = connection.curso;
        let usuario = connection.usuario;
        let sesion = this.salas[curso].sesion;

        if(sesion.usuario_moderador.id === usuario.id){

            sesion.expulsarUsuario(data.usuarioExpulsado.nombre_usuario)
                .then(() => {

                    // se elimina la conexion
                    delete self.salas[curso]['conexiones'][data.usuarioExpulsado.nombre_usuario];

                    // se notifica el abandono
                    this.currentState(connection.idSesion, usuario.id)
                        .then((state) => {
                            _.forIn(self.salas[curso]['conexiones'], (connection) => {
                                InfoMeeting.SendTo(connection, {
                                    type: "leave",
                                    state: state,
                                    disconnectedUser: data.usuarioExpulsado
                                });
                            });
                        })
                        .catch((error) => console.log(error));
                })
                .catch((error) => console.log(error));

        }

    }


    /** Cambio de modo **/

    handleChangeMode(connection, data){
        let self = this;
        let cusrso = connection.curso;
        let sesion = self.salas[cusrso].sesion;

        sesion.cambiarModo(data.mode, data.user)
            .then(() => {
                sesion.modo_actual = data.mode;
                _.forIn(self.salas[cusrso]['conexiones'], function (connection) {
                    InfoMeeting.SendTo(connection, {
                        type: "changeMode",
                        mode: data.mode,
                    });
                });
            })
            .catch((error) => {console.log(error)})

    }

    handleEditor(connection, data){
        let curso = connection.curso;
        this.salas[curso].sesion.contenido_editor = data.text;
    }


    /** Chats individuales y generales **/

    handleChat(connection, data){

        let self = this;
        let curso = connection.curso;
        let sesion = self.salas[curso].sesion;

        if(data.chat === 'main'){
            sesion.enviarMensajeGeneral(data.user.nombre_usuario, data.msg)
                .then(() => {
                    _.forIn(self.salas[curso]['conexiones'], function (connection) {
                        InfoMeeting.SendTo(connection, {
                            type: "chat",
                            chat: 'main',
                            lines: [{
                                usuario: data.user.nombre_usuario,
                                mensaje: data.msg
                            }]
                        });
                    });
                })
                .catch((error) => console.log(error));
        }else{
            sesion.enviarMensajeUsuario(data.usuarioDesde.nombre_usuario, data.usuarioHacia.nombre_usuario, data.msg)
                .then(() => {
                    InfoMeeting.SendTo(self.salas[curso]['conexiones'][data.usuarioDesde.nombre_usuario], {
                        type: "chat",
                        chat: 'private_sent',
                        lines: [{
                            usuario: data.usuarioHacia.nombre_usuario,
                            mensaje: data.msg
                        }]
                    });

                    InfoMeeting.SendTo(self.salas[curso]['conexiones'][data.usuarioHacia.nombre_usuario], {
                        type: "chat",
                        chat: 'private_received',
                        lines: [{
                            usuario: data.usuarioDesde.nombre_usuario,
                            mensaje: data.msg
                        }]
                    });

                })
                .catch((error) => console.log(error));
        }
    }

    handleIndividualChat(connection, data){

        DB.query({
            sql: 'select nombre_usuario as usuario, mensaje from chats_individuales ' +
            'inner join usuarios on usuarios.id = chats_individuales.id_usuario_emisor ' +
            'where id_sesion = ? and ((id_usuario_emisor = ? and id_usuario_receptor = ?) or (id_usuario_emisor = ? and id_usuario_receptor = ?))',
            values: [connection.idSesion, connection.usuario.id, data.usuario.id, data.usuario.id, connection.usuario.id]
        }, function (error, results) {
            InfoMeeting.SendTo(connection, {
                type: 'chatIndividual',
                chats: results
            });
        })
    }


    /** Encuestas **/

    handleCreatePoll(connection, data){

        let self = this;
        let curso = connection.curso;
        let encuesta = new Encuesta();
        encuesta.usuario = connection.usuario;
        encuesta.sesion = self.salas[curso].sesion;
        encuesta.pregunta = data.poll;

        encuesta.crear()
            .then((encuesta) => {

                return self.getDatosEncuestas(connection.idSesion, connection.usuario.id)
                    .then((encuestas) => {
                        _.forIn(self.salas[curso]['conexiones'], function (connection) {
                            InfoMeeting.SendTo(connection, {
                                type: "encuestas",
                                encuestas: encuestas,
                            });
                        });
                    })
                    .catch((error) => console.log(error));

            })
            .catch((error) => console.log(error));

    }

    handlePollAnswer(connection, data){

        let self = this;
        let curso = connection.curso;
        let sesion = self.salas[curso].sesion;

        DB.query({
            sql: 'insert into encuestas_respuestas (id_encuesta, id_usuario, respuesta) values(?,?,?)',
            values: [data.idEncuesta, data.usuario.id, data.respuesta]
        }, function (error) {
            if(error) reject(error);
            let rtaEncuestas;
            self.getDatosEncuestas(sesion.id, connection.usuario.id)
                .then((encuestas) => {
                    rtaEncuestas = encuestas;
                    return self.getDatosRespuestas(sesion.id);
                })
                .then((respuestas) => {

                    InfoMeeting.SendTo(connection, {
                        type: "encuestas",
                        encuestas: rtaEncuestas,
                        respuestas: respuestas
                    });


                    _.forIn(self.salas[curso]['conexiones'], function (connection) {
                        if(data.usuario.id !== connection.usuario.id){
                            InfoMeeting.SendTo(connection, {
                                type: "respuestas",
                                respuestas: respuestas,
                                usuario: data.usuario
                            });
                        }

                    });
                })
                .catch((error) => console.log(error));
        })
    }


    /** Funciones utiles para retornar datos del estado de la aplicacion **/

    getDatosSesion(idSesion) {
        let self = this;
        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'select * from sesiones where id = ?',
                values: [idSesion]
            }, (error, results) => {
                if(error) reject(error);
                else if(results[0] === undefined) reject('El id de sesion buscado no trajo resultados');
                else {
                    let curso = results[0].curso;
                    // actualizo el contenido del editor con lo que hay en memoria
                    if(self.salas[curso] && self.salas[curso]['sesion']){
                        results[0].contenido_editor = self.salas[curso]['sesion'].contenido_editor;
                    }
                    fulfill(results[0])

                }
            });
        });
    }

    getDatosUsuario(sesion, property) {
        let self = this;
        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'select * from usuarios where id = ?',
                values: [sesion[property]]
            }, (error, results) => {
                if(error) reject(error);
                else if(results[0] === undefined) reject('El id de usuario buscado no trajo resultados');
                else {
                    sesion[property] = results[0];
                    fulfill(sesion)
                }
            });
        });
    }


    getDatosEncuestas(idSesion, idUsuario) {
        if(idUsuario){
            return new Promise((fulfill, reject) => {
                DB.query({
                    sql: 'select * from encuestas\n' +
                    'where id_sesion = ? and not exists \n' +
                    '(select * from encuestas_respuestas where id_usuario = ? and id_encuesta = encuestas.id)',
                    values: [idSesion, idUsuario]
                }, function (error, results) {
                    if(error) reject(error);
                    else fulfill(results);
                });
            });
        }else{
            return new Promise((fulfill, reject) => {
                DB.query({
                    sql: 'select * from encuestas\n' +
                    'where id_sesion = ?',
                    values: [idSesion]
                }, function (error, results) {
                    if(error) reject(error);
                    else fulfill(results);
                });
            });
        }

    }

    getDatosRespuestas(idSesion) {
        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'select encuestas.id, encuestas.pregunta, count(respuesta) as total, sum(respuesta) as si from encuestas_respuestas\n' +
                     'inner join encuestas on encuestas.id = encuestas_respuestas.id_encuesta\n' +
                     'where encuestas.id_sesion = ? \n' +
                     'group by id_encuesta',
                values: [idSesion]
            }, function (error, results) {
                if(error) reject(error);
                else fulfill(results);
            });
        });
    }

    getDatosUsuarios(idSesion) {

        return new Promise((fulfill, reject) => {
            DB.query({
                sql: 'select usuarios.*, su.pide_palabra from usuarios\n' +
                'inner join sesiones_usuarios su on su.id_usuario = usuarios.id\n' +
                'where su.id_sesion = ? and fecha_desconexion is null',
                values: [idSesion]
            }, function (error, results) {
                if(error) reject(error);
                else fulfill(results);
            });

        });
    }

    currentState(idSession, idUsuario){

        let datos =   {
            mainChat: '',
            encuestas: {},
            sesion: {},
            usuarios: {},
        };

        return this.getDatosSesion(idSession)
            .then((sesion) => {
                return this.getDatosUsuario(sesion, 'usuario_activo');
            })
            .then((sesion) => {
                return this.getDatosUsuario(sesion, 'usuario_moderador');
            })
            .then((sesion) => {
                datos.sesion = sesion;
                return this.getDatosUsuarios(idSession);
            })
            .then((usuarios) => {
                datos.usuarios = usuarios;
                return this.getDatosEncuestas(idSession, idUsuario)
            })
            .then((encuestas) => {
                datos.encuestas = encuestas;
                return this.getDatosRespuestas(idSession);
            })
            .then((respuestas) => {
                datos.respuestas = respuestas;
                return datos;
            })
            .catch((error) => console.log(error));
    }


    static SendTo(connection, message){
        if(connection && connection.readyState === 1){
            connection.send(JSON.stringify(message));
        }
    }

    static GetSesiones(){

        return new Promise((fulfill) => {
            Sesion.All()
                .then((rows) => fulfill(rows));
        });
    }

    GetSesion(id){

        let data = null;

        return new Promise((fullfill, reject) => {
            this.getDatosSesion(id)
                .then((sesion) => {

                    data = sesion;

                    (async function fetchData() {

                        // usuarios
                        await Usuario.AllInSession(id).then((usuarios) => data.usuarios = usuarios);

                        // encuestas
                        let encuestas = [];
                        await Encuesta.AllInSession(id).then((results) => encuestas = results);

                        // respuestas
                        for (const encuesta of encuestas) {
                            await Encuesta.AllRespuestas(encuesta.id_encuesta)
                                .then(respuestas => encuesta.respuestas = respuestas)
                        }
                        data.encuestas = encuestas;

                        // chats
                        await Sesion.ChatGeneral(id).then((chats) => data.chat_general = chats);

                        // chats individuales
                        await Sesion.ChatIndividuales(id).then((chats) => data.chats_individuales = chats);

                        // interacciones individuales
                        data.interacciones = {};
                        await Sesion.Interacciones(id, 'cambio modo').then((results) => data.interacciones.cambios_de_modo = results);
                        await Sesion.Interacciones(id, 'pedir control').then((results) => data.interacciones.pedidos_de_palabra = results);
                        await Sesion.Interacciones(id, 'otorgar control').then((results) => data.interacciones.asignaciones_de_palabra = results);
                        await Sesion.Interacciones(id, 'recuperar control').then((results) => data.interacciones.recuperaciones_de_palabra = results);
                        await Sesion.Interacciones(id, 'expulsion').then((results) => data.interacciones.bloqueos = results);

                        fullfill(data);
                    })();
                })
                .catch((error) => reject(error));
        })
    }

}

export default InfoMeeting;