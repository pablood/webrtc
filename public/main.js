import _ from 'lodash';
import * as ViewUsersPanel from './view-users-panel';
import * as ViewPolls from './view-polls';
import * as ViewChat from './view-chat';
import config from '../config';

//const iceServers =  { 'iceServers': [{"urls":["stun:stun.l.google.com:19302"]}] };

const iceServers =  { 'iceServers': [
    {"urls": ["turn:numb.viagenie.ca"],"username": "webrtc@live.com", "credential":"muazkh"},
    {"urls": ["stun:stun.l.google.com:19302"]}
]};

let user = null;
let selectedUser = null;
let conn = new WebSocket(getSocketUrl());
let rtcConnections = [];
let rtcDataChannels = []; // for the editor
let streamLocal;
let streamRemotes = [];
let appState;
let codeMirror = null;
let userOnMainVideo;

const privateAudio = new Audio('sounds/private.wav');
const generalAudio = new Audio('sounds/general.wav');

class InfoMeetingUI {

    constructor(){
        this.chatGeneral = '#primary-chat';
        this.video = '#remoteVideo';
        this.editor = '.CodeMirror';
    }

    closeMobileMenu(){
        if($('body').hasClass('show-sidebar')){
            $('#mobile-menu-button').trigger('click');
        }
    }

    notifyNewChat(){
        if(this.isPanelCollapsed(this.chatGeneral)){
            this.blinkPanel(this.chatGeneral)
        }
    }

    blinkPanel(selector){
        $(selector).find('.panel-heading').css('background-color', '#ffb606');
        setTimeout(() => $(selector).find('.panel-heading').css('background-color', ''), 500);
    }

    isPanelCollapsed(selector) {
        return $(selector).hasClass('collapsed') || $(selector).hasClass('panel-collapse');
    }

    toggleVideoEditor(){
        if(appState.sesion.modo_actual === 'video'){
            $(this.video).show().closest('.panel-body').removeClass('mode-editor');
            $(this.editor).hide();
        }else if(appState.sesion.modo_actual === 'editor'){
            $(this.video).hide().closest('.panel-body').addClass('mode-editor');
            $(this.editor).show();
            setTimeout(() => codeMirror.refresh(), 1);
        }
    }

    renderMobile() {
        if(window.innerWidth <= 768){
            $('#menu').show();
            $('#header, #menu, #wrapper').wrapAll('<div class="boxed-wrapper"></div>');
            $('.header-link').show();

            // collapse panels
            $('#primary-chat').toggleClass('collapsed');
            $('#panel-participantes').toggleClass('collapsed');
        }
    }

    renderMobileMenu() {

        let $cerarSesion = $("<a>", {href: "/logout.html", html: '<i class="fa fa-sign-out"></i> <span>Cerrar Sesión</span>'});

        let $audio = $("<a>", {id:"toggle-audio-mobile", href: "#", html: '<i class="fa fa-microphone"></i> <span style="font-size: .9em">Desactivar Audio</span>'});

        let $video = $("<a>", {id:"toggle-video-mobile", href: "#", html: '<i class="fa fa-video-camera"></i> <span style="font-size: .9em">Desactivar Video</span>'});

        let $pedirLaPalabra = $("<a>", {href: "javascript:;", html: '<i class="pe-7s-speaker"></i> <span class="toggle-request-control">Pedir la Palabra</span>'});

        let $nuevaEncuesta = $("<a>", {href: "#", html: ' <i class="pe pe-7s-comment text-info"></i><span class="nav-label">Nueva Consulta</span>' })
            .attr('data-toggle', 'modal')
            .attr('data-target', '#myModal');

        let $verResultados = $("<a>", {id: 'btn-open-resultados-mobile', href: "#myModal2", html: '<i class="pe pe-7s-graph1 text-danger"></i> <span class="nav-label">Ver Resultados</span>' })
            .attr('data-toggle', 'modal');

        let $consultas = $("<a>", {href: "#", html: ' <span class="nav-label">Consultas</span>'}).attr('aria-expanded', true);

        let $ul2 = $("<ul>", {href: "#", class:"nav nav-second-level collapse in ul-encuestas"}).attr('aria-expanded', true);

        if(appState.encuestas.length){
            appState.encuestas.map((e) => {
                let preguntaTruncada = ViewPolls.truncate.apply(e.pregunta, [50, true]);
                $ul2.append(`<li><a class="swal-encuesta" href="javascript:;" data-id="${e.id}" data-pregunta="${e.pregunta}">${preguntaTruncada}</a></li>`);
            });
        }else{
            $ul2.append('<li class="empty-polls">No hay consultas pendientes</li>');
        }


        $("#side-menu").empty();
        $("#side-menu").append($cerarSesion, $pedirLaPalabra, $audio, $video, $nuevaEncuesta, $verResultados, $consultas);

        $cerarSesion.wrap('<li></li>');
        $audio.wrap('<li></li>');
        $video.wrap('<li></li>');
        $pedirLaPalabra.wrap('<li></li>');
        $nuevaEncuesta.wrap('<li></li>');
        $verResultados.wrap('<li></li>');
        $consultas.wrap('<li class="active"></li>');
        $consultas.after($ul2);
        if(isAdmin()){
            $('#side-menu > li:nth-child(2)').hide()
        }

    }

    renderInitialState(){
        ui.renderMobile();
        ViewUsersPanel.render(appState, user, addStream);
        ui.renderMobileMenu();
        ViewPolls.render(appState.encuestas, user, send);
        ViewPolls.renderRespuestas(appState.respuestas);

        $('#editorarea').html(appState.sesion.contenido_editor);

        if(appState.sesion.usuario_activo.id){
            $('#label-main-component').html(appState.sesion.usuario_activo.nombre_completo);
        }

        initEditor();

        if(isAdmin()){
            $('#btn-eject').show();
            $('#btn-give-control').show();
            $('#panel-main-video .panel-tools').show();
        }else{
            $('#icon-toggle-request-control').show();
        }
    }

    showUserChat(usuario){
        $('#private-chat-label').html(usuario.nombre_completo);
        $('#private-chat').empty();
        $(`.chat-user[data-username="${usuario.nombre_usuario}"] span.label-warning`).remove();
        $('#secondary-chat').show();
        $('#primary-chat .panel-body').css('height', '25%');
        send({type: 'individualChat', usuario: usuario});
        $('html, body').animate({
            scrollTop: $("#secondary-chat").offset().top - 70
        }, 1000);
    }

    showIncomingMessageNotification(username){

        let toastrClick = () => {
            let indiceUsuario = $(`#RXUsers .chat-user[data-username="${username}"]`).data('userindex');
            selectedUser = appState.usuarios[indiceUsuario];
            ui.showUserChat(selectedUser);
            $(`.chat-user[data-username="${username}"] span.label-warning`).remove();
        };
        toastr.options = { onclick: toastrClick};
        toastr["warning"]("Mensaje de " + username);

        $(`.chat-user[data-username="${username}"] span.label-warning`).remove();
        $(`.chat-user[data-username="${username}"]`).prepend('<span class="label label-warning pull-right">Nuevo Mensaje</span>')
    }

    showNewAnswerNotification(username){
        toastr["warning"]("Nueva respuesta de " + username);
    }

}



const ui = new InfoMeetingUI();


conn.onerror = (event) => console.log(event) ;

conn.onopen = (event) => console.log('Connected to 9090');

conn.onclose = (event) => console.log('Lost connection to 9090', event);

conn.onmessage =  function (response) {

    let data = JSON.parse(response.data);
    let isDataMessage = data.type !== 'offer' && data.type !== 'answer' && data.type !== 'candidate';
    if(isDataMessage) console.log('IN: ' + data.type, data);

    switch (data.type){

        // connection related messages
        case 'login': handleLogin(data); break;
        case 'offer': handleOffer(data); break;
        case 'answer': handleAnswer(data); break;
        case 'candidate': handleCandidate(data); break;
        case 'leave': handleLeave(data); break;

        // application related messages
        case 'newuser': handleNewUser(data); break;
        case 'changeActiveUser': handleChangeActiveUser(data); break;
        case 'requestControl': handleRequestControl(data); break;
        case 'changeMode': handleChangeMode(data); break;
        case 'chat': handleChat(data); break;
        case 'chatIndividual': handleChatIndividual(data); break;
        case 'pollAnswer': handlePollAnswer(data); break;
        case 'encuestas': handleEncuestas(data); break;
        case 'respuestas': handleRespuestas(data); break;
        default: break;
    }
};

$(document).ready(() => {
    let token = getParameterByName('token');
    if($.isEmptyObject(token)) console.log('Token invalido');
    send({type: "login", token: token});

});



function init(user) {

    $('#RXUsers').on('click', '.chat-user a', function() {
        let selectedIdx = $(this).closest('.chat-user').data('userindex');
        if(selectedUser === null || selectedUser.id !== appState.usuarios[selectedIdx].id){
            selectedUser = appState.usuarios[selectedIdx];
            if(selectedUser.id !== user.id){
               ui.showUserChat(selectedUser);
            }
        }
    });

    $('#RXUsers').on('click', '.btn-dar-control', function () {
        let selectdIdx = $(this).closest('.chat-user').data('userindex');
        let usuarioADarControl = appState.usuarios[selectdIdx];
        changeActiveUser(usuarioADarControl);
    });

    $('#RXUsers').on('click', '.btn-expulsar', function () {
        let selectdIdx = $(this).closest('.chat-user').data('userindex');
        let usuarioAExpulsar = appState.usuarios[selectdIdx];
        expulsarUsuario(usuarioAExpulsar);
    });

    $('#btn-give-control').on('click', () => changeActiveUser());

    $('#btn-eject').on('click', () => expulsarUsuario());

    $('body').on('click', '.toggle-request-control', () => {

        if(!isAdmin()){

            ui.closeMobileMenu();

            if(hasControl()){
                send({type: 'changeActiveUser', usuarioHacia: appState.sesion.usuario_moderador, usuarioDesde: user});
            }else{
                if(appState.sesion.usuario_moderador.id){
                    send({type: 'requestControl', user: user});
                    swal({
                        title: "Pedir la Palabra",
                        text: "Su pedido ha sido enviado al tutor."
                    });
                }
                // todo remove else
                else{
                    swal({
                        title: "Pedir la Palabra",
                        text: "Por favor espere a que un tutor se una a la sesión."
                    });
                }
            }

        }});

    $('#toggle-mode-editor').on('click', () => {
        if(appState.sesion.modo_actual !== undefined && appState.sesion.modo_actual !== 'editor'){
            send({ type: 'changeMode', mode: 'editor', user: user})
        }
    });
    $('#toggle-mode-video').on('click', () => {
        if(appState.sesion.modo_actual !== undefined && appState.sesion.modo_actual !== 'video'){
            send({ type: 'changeMode', mode: 'video', user: user})
        }
    });


    // set main chat events

    $('#chat-input').keypress((e) => { if(e.which === 13) $('#chat-btn').trigger('click')} );

    $('#private-chat-input').keypress((e) => { if(e.which === 13) $('#private-chat-btn').trigger('click')} );

    $('#chat-btn').on('click', () => {
        let $chatInput = $('#chat-input');
        if($chatInput.val().length){
            send({ type: 'chat', user: user, msg: $chatInput.val(), chat: 'main' });
            $chatInput.val('');
        }
    });

    // set individual chat events
    $('#private-chat-btn').on('click', () => {
        let $privateChatInput = $('#private-chat-input');
        if($privateChatInput.val().length){
            send({ type: 'chat', usuarioDesde: user, usuarioHacia: selectedUser, msg: $privateChatInput.val(), chat: 'individual' });
            $privateChatInput.val('');
        }
    });

    $('#btn-close-secondary-chat').on('click', () => {
        selectedUser = null;
        $('#secondary-chat').hide();
        $('#private-chat').empty();
        $('#primary-chat .panel-body').css('height', '65%');
    });

    // set local video events

    $('body').on('click', '#btn-toggle-audio, #toggle-audio-mobile', () => {

        // desactivar
        if(streamLocal.getAudioTracks()[0].enabled){
            $('#btn-toggle-audio h5').html('Activar Audio');
            $('#btn-toggle-audio i').css('color', 'inherit');
            $('#toggle-audio-mobile span').html('Activar Audio');
        }

        // activar
        else{
            $('#btn-toggle-audio h5').html('Desctivar Audio');
            $('#btn-toggle-audio i').css('color', '#5BC0DE');
            $('#toggle-audio-mobile span').html('Desactivar Audio');

        }
        streamLocal.getAudioTracks()[0].enabled = !(streamLocal.getAudioTracks()[0].enabled);
    });

    $('body').on('click','#btn-toggle-video, #toggle-video-mobile', () => {

        // desactivar
        if(streamLocal.getVideoTracks()[0].enabled){
            $('#btn-toggle-video h5').html('Activar Cámara');
            $('#btn-toggle-video i').css('color', 'inherit');
            $('#toggle-video-mobile span').html('Activar Video');
        }

        // activar
        else{
            $('#btn-toggle-video h5').html('Desactivar Cámara');
            $('#btn-toggle-video i').css('color', '#5BC0DE');
            $('#toggle-video-mobile span').html('Desactivar Video');
        }
        streamLocal.getVideoTracks()[0].enabled = !(streamLocal.getVideoTracks()[0].enabled);
    });

    // set poll events
    $('#btn-create-poll').on('click', () => {
        var $pollDescription = $('#poll-description').val();
        if(!$.isEmptyObject($pollDescription)) send({type: 'createPoll', poll: $pollDescription, user: user});
        $('#poll-description').empty();
        ui.closeMobileMenu();
    });

    $('#RXPolls').on('pollAnswer', (e, poll, answer) => send({ type: 'pollAnswer', poll: poll, answer: answer, user: user }));

}

function handleLogin(data){
    if(data.success) {
        appState = data.state;
        user = data.connectedUser;
        init(user);
        ui.renderInitialState();

        let userIsModerador = appState.sesion.usuario_moderador.id === user.id;

        // inicio el stream local
        navigator.mediaDevices.getUserMedia({ video: true, audio: true})
            .then((stream) => {
                streamLocal = stream;

                if(config.modo === 'video'){
                    if(userIsModerador){
                        const vid = document.getElementById('remoteVideo');
                        vid.autoplay = true;
                        vid.muted = true;
                        vid.srcObject = stream;
                    }
                    const vid = document.getElementById('localVideo');
                    vid.autoplay = true;
                    vid.muted = true;
                    vid.srcObject = stream;
                }else{
                    if(userIsModerador){
                        $('#remoteVideo').attr('poster', 'https://dummyimage.com/600x400/fff/000&text=Tú')
                    }
                    $('#localVideo').attr('poster', 'https://dummyimage.com/600x400/fff/000&text=Tú');
                }

                // creo una conexion con cada miembro y le envio una oferta
                _.forIn(data.state.usuarios, (usuario) => {
                    createNewRtcConnection(usuario, true);
                });

        })
            .catch((err) =>{
                console.log(err);
                alert('No se pudo obtener acceso a la videocamara o al micrófono. Si desea intervenir en la sesión, habilite los dispositivos y vuelva a cargar la página')

                streamLocal = null;
                $('#localVideo').attr('poster', 'https://dummyimage.com/600x400/fff/000&text=Tú');
                if(userIsModerador){
                    $('#remoteVideo').attr('poster', 'https://dummyimage.com/600x400/fff/000&text=Tú');
                }
                // creo una conexion con cada miembro y le envio una oferta
                _.forIn(data.state.usuarios, (usuario) => {
                    createNewRtcConnection(usuario, true);
                });
            });
    }else{
        console.log('Error en el login');
        console.log(data);
        window.location.assign('404.html');
    }
}

function handleOffer(data) {
    rtcConnections[data.name].setRemoteDescription(new RTCSessionDescription(data.offer));
    rtcConnections[data.name].createAnswer((answer) => {
        rtcConnections[data.name].setLocalDescription(answer);
        send({type: 'answer', answer: answer, from: user, toName: data.name})
    }, (err) => console.log('error on answer: ', err));
}

function handleAnswer(data) {
    rtcConnections[data.name].setRemoteDescription(new RTCSessionDescription(data.answer));
}

function handleCandidate(data) {
    if (rtcConnections[data.name]) {
        rtcConnections[data.name]
            .addIceCandidate(new RTCIceCandidate(data.candidate))
            .catch(e => console.log('GOT ERROR', e.message));
    }
}


let addStream = (participant) =>{
    const vid = document.querySelector(`div[data-username="${participant}"] video`);
    vid.autoplay = true;
    if(streamRemotes[participant]){
        vid.muted = participant === appState.sesion.usuario_activo.nombre_usuario;
        vid.srcObject = streamRemotes[participant];
    }else if(participant === user.nombre_usuario){
        vid.muted = true;
        vid.srcObject = streamLocal;
    }
};



function handleNewUser(data) {
    appState = data.state;
    ViewUsersPanel.render(appState, user, addStream);
    createNewRtcConnection(data.connectedUser);
    toastr["warning"](data.connectedUser.nombre_completo + " se ha unido a la sesión");
}

function createNewRtcConnection(usuarioRemoto, shouldSendOffer) {

    let nombreUsuarioRemoto = usuarioRemoto.nombre_usuario;

    if(nombreUsuarioRemoto === user.nombre_usuario) return false;

    let newConnection = new RTCPeerConnection(iceServers);

    // configure stream for video
    newConnection.addStream(streamLocal);
    newConnection.onaddstream = function (e) {

        // los usuarios entran en este orden: pablo, cecilia, gladys

        // caso 1: entra cecilia y pablo es moderador: el video sigue mostrando a pablo
        // caso 2: entra cecilia y ella es la moderadora: el video pasa a cecilia
        // caso 3: entra cecilia y gladys es la moderadora: no hay cambios

        streamRemotes[nombreUsuarioRemoto]= e.stream;
        addStream(nombreUsuarioRemoto);
        let entraElModerador = appState.sesion.usuario_moderador.id === usuarioRemoto.id;
        if(entraElModerador){
            updateMainVideo();
            userOnMainVideo = nombreUsuarioRemoto;
        }
    };

    // configure signaling @TODO se podra poner antes del stream?
    newConnection.onicecandidate = function (event) {
        if(event.candidate){
            send({
                type: 'candidate',
                candidate: event.candidate,
                fromName: user.nombre_usuario
            });
        }
    };
    newConnection.ondatachannel = function(event){
        let receiveChannel = event.channel;
        receiveChannel.onopen = function (e) {
            console.log('Channel opened', e)
        };
        receiveChannel.onmessage = function (e) {
            codeMirror.setValue(e.data);
        };
        receiveChannel.onerror = function (err) {
            console.log('Got error on dataChannel: ', err);
        };
        receiveChannel.onclose =  function () {
            console.log('DataChannel is closed');
        };
    };

    // configure dataChannel for editor
    rtcDataChannels[nombreUsuarioRemoto] = newConnection.createDataChannel("textChannel", {reliable: true});

    // save the rtc connection
    rtcConnections[nombreUsuarioRemoto] = newConnection;

    // send offer only on login, not on new user
    if(shouldSendOffer){
        sendOffer(nombreUsuarioRemoto);
    }

}

function sendOffer(nombreUsuarioRemoto) {
    if(nombreUsuarioRemoto === user.nombre_usuario) return false;
    rtcConnections[nombreUsuarioRemoto].createOffer(function (offer) {
        send({type: 'offer', offer: offer, from: user, to: nombreUsuarioRemoto});
        rtcConnections[nombreUsuarioRemoto].setLocalDescription(offer)
    }, function (err) {
        console.log('error creating offer:', err)
    })
}

function changeActiveUser(usuario) {
    let usuarioADarControl = (usuario) ? usuario : selectedUser;
    if(isAdmin()) {
        // dar control
        if(usuarioADarControl.id !== appState.sesion.usuario_activo.id){
            send({type: 'changeActiveUser', usuarioHacia: usuarioADarControl, usuarioDesde: user});
        }
        // recuperar control
        else{
            send({type: 'changeActiveUser', usuarioHacia: user, usuarioDesde: user});
        }
    }
}

function expulsarUsuario(usuario) {
    if(isAdmin()) {

        swal({
                title: "Bloquear Usuario",
                text: "Se bloquerá la participación del usuario",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Bloquear",
                cancelButtonText: "Cancelar"
            },
            function () {
                if(usuario){
                    // se ejecuto la accion desde el sidebar
                    send({type: 'expulsarUsuario', usuarioExpulsado: usuario, user: user});
                }
                else{
                    // se ejecuto la accion desde la ventana de chat
                    send({type: 'expulsarUsuario', usuarioExpulsado: selectedUser, user: user});
                }
            });
    }
}

function handleLeave(data){
    if(data.disconnectedUser.nombre_usuario === user.nombre_usuario){
        window.location.assign('/logout.html')
    }
    rtcConnections[data.disconnectedUser.nombre_usuario] = null;
    rtcDataChannels[data.disconnectedUser.nombre_usuario] = null;
    streamRemotes[data.disconnectedUser.nombre_usuario] = null;

    appState = data.state;
    updateMainVideo();
    $('#label-main-component').html(appState.sesion.usuario_activo.nombre_completo);
    userOnMainVideo = appState.sesion.usuario_activo.nombre_usuario;
    initEditor();
    ViewUsersPanel.render(appState, user, addStream);
    toastr["warning"](data.disconnectedUser.nombre_completo + " ha salido de la sesión");
}

function handleChangeActiveUser(data) {

    let hayCambiodeUsuario = appState.sesion.usuario_activo.id !== data.state.sesion.usuario_activo.id;
    let usuarioActualTeniaElControl = appState.sesion.usuario_activo.id === user.id;

    if (hayCambiodeUsuario) {
        // confirmacion al tutor de que la transferencia de control se hizo correctamente
        if (isAdmin()) {
            swal({
                title: 'Transferencia de control',
                text: 'El control ha sido transferido'
            });
        }
        // se le notifica al usuario que tenia el control, que ya no lo tiene
        else if (usuarioActualTeniaElControl) {
            swal({
                title: 'Ya no tiene la palabra',
                text: 'El tutor ha transferido el control a otro usuario'
            });
            $('.toggle-request-control').html('Pedir la palabra')
        }

        appState = data.state;
        if (appState.sesion.usuario_activo.id === user.id) {

            if(isAdmin()){
                swal({ title: 'Tiene la palabra recuperada', text: 'La palabra ha sido recuperada' });
            }else {
                swal({ title: 'Tiene la palabra', text: 'El tutor te ha transferido la palabra' });
                $('.toggle-request-control').html('Devolver la palabra');
            }

            $('#panel-main-video .panel-tools').show();
        }else{
            $('#panel-main-video .panel-tools').hide();
        }

        updateMainVideo();

        userOnMainVideo = appState.sesion.usuario_activo.nombre_usuario;

        initEditor();
        ViewUsersPanel.render(appState, user, addStream);
    }
}

function updateMainVideo(){
    let newUser = appState.sesion.usuario_activo;

    // no soy el usuario activo
    if(user.id !== newUser.id){

        $('#icon-toggle-request-control').show();

        // muestro el stream remoto en el video principal
        if(config.modo === 'poster'){
            $('#remoteVideo').attr('poster', 'https://dummyimage.com/600x400/fff/000&text=' + newUser.nombre_completo)
        }else{
            const vid = document.getElementById('remoteVideo');
            vid.autoplay = true;
            vid.muted = false;
            vid.srcObject = streamRemotes[newUser.nombre_usuario];
        }
    }
    // soy el usuario activo
    else {

        // muestro el stream local en el video principal
        if(config.modo === 'poster'){
            $('#remoteVideo').attr('poster', 'https://dummyimage.com/600x400/fff/000&text=Tú')
        }else{
            const vid = document.getElementById('remoteVideo');
            vid.autoplay = true;
            vid.muted = true;
            vid.srcObject = streamLocal;
        }
    }

    $('#label-main-component').html(appState.sesion.usuario_activo.nombre_completo);
}

function handleRequestControl(data){
    appState = data.state;
    ViewUsersPanel.render(appState, user, addStream);
}

function handleChat(data) {
    if(data.chat === 'private_received'){

        let nombreUsuario = data.lines[0].usuario;
        // si el chat no esta visible
        if(!selectedUser || selectedUser.nombre_usuario !== nombreUsuario){
            ui.showIncomingMessageNotification(nombreUsuario);
            privateAudio.play();
        }
        // el chat del usuario ya esta visible
        else{
            ViewChat.renderIndividual(data.lines);
        }

    }
    else if(data.chat === 'private_sent'){
        let nombreUsuario = data.lines[0].usuario;

        // actualizamos el label
        data.lines[0].usuario = user.nombre_completo;

        // si el chat esta visible, agregamos la nueva linea
        if(selectedUser && selectedUser.nombre_usuario === nombreUsuario){
            ViewChat.renderIndividual(data.lines);
        }
    }
    else{
        ViewChat.renderGeneral(data.lines);
        
        // sound and visual notifications
        if(data.initial === undefined){
            generalAudio.play();
            ui.notifyNewChat();
        }
        
    }
}

function handleChatIndividual(data) {
    ViewChat.renderIndividual(data.chats);
}

function handleEncuestas(data) {
    appState.encuestas = data.encuestas;
    appState.respuestas = data.respuestas;
    ui.renderMobileMenu();
    ViewPolls.render(appState.encuestas, user, send);
    ViewPolls.renderRespuestas(appState.respuestas);
}

function handleRespuestas(data) {
    appState.respuestas = data.respuestas;
    ui.renderMobileMenu();
    ViewPolls.renderRespuestas(appState.respuestas);
    ui.showNewAnswerNotification(data.usuario.nombre_usuario);
}

function handlePollAnswer(data){
    ViewPolls.renderPoll(data.poll, data.results)
}

function handleChangeMode(data){
    appState.sesion.modo_actual = data.mode;
    ui.toggleVideoEditor();
}

function initEditor() {

    // inicia el editor por primera vez
    if(codeMirror === null){
        codeMirror = CodeMirror.fromTextArea(document.getElementById('editorarea'), {
            lineNumbers: true,
            matchBrackets: true,
            styleActiveLine: true,
            readOnly: user.id !== appState.sesion.usuario_activo.id,
            value: appState.sesion.contenido_editor
        });
        codeMirror.on('keyup', (instance, event) => {
            if(instance.options.readOnly === false){
                let msg = codeMirror.getValue();
                _.forIn(rtcDataChannels, (value, key) => { if(value !== null) value.send(msg) });
                send({type: 'editor', text: msg});
            }
        })

    }
    // el editor ya esta instanciado, actualizamos la propiedad readonly
    else{
        codeMirror.setOption('readOnly' , user.id !== appState.sesion.usuario_activo.id)
    }

    ui.toggleVideoEditor();

}

function send(message){
    let isDataMessage = message.type !== 'offer' && message.type !== 'answer' && message.type !== 'candidate';
    if(isDataMessage) console.log('OUT: ' + message.type, message);
    waitForSocketConnection(conn, () => conn.send(JSON.stringify(message)) );
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function isAdmin() {
    return user.id === appState.sesion.usuario_moderador.id;
}

function hasControl() {
    return (appState.sesion.usuario_activo.id && appState.sesion.usuario_activo.id  === user.id);
}

function waitForSocketConnection(socket, callback){
    setTimeout(() => {
        if (socket.readyState === 1) {
            if(callback !== null) callback();
        } else {
            console.log("wait for connection...");
            waitForSocketConnection(socket, callback);
        }
    }, 2000); // wait for the connection...
}

function getSocketUrl(){
    return  config.webSocketUrl;
}


