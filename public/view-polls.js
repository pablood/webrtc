let _ = require('lodash');

export function render(polls, user, fnSend) {
    if(polls.length){

        $('#panel-encuestas a span').html(polls.length);
        $('.header-link.hide-menu span.label').html(polls.length);
        $('#panel-encuestas ul li').remove();
        $('#panel-encuestas ul div').html(`Tiene ${polls.length} encuestas sin responder`);
        _.forIn(polls, (poll) => append(poll) );
    }else{
        $('#panel-encuestas a span').empty();
        $('.header-link.hide-menu span.label').empty();
        $('#panel-encuestas ul').empty();
        $('#panel-encuestas ul').html('<div class="title">No hay encuestas para responder</div>');
    }

    $('.swal-encuesta').on('click', (e) => {
        // close mobile menu
        if($('body').hasClass('show-sidebar')){
            $('#mobile-menu-button').trigger('click');
        }
        let pregunta = $(e.target).data('pregunta');
        let idEncuesta = $(e.target).data('id');

        swal({
                title: 'Nueva Encuesta',
                text: pregunta,
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#62CB31",
                cancelButtonColor: "#DD6B55",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
                    fnSend({type: 'pollAnswer', respuesta: true, usuario: user, idEncuesta: idEncuesta});
                    swal("Gracias!", "Tu respuesta fue: Si", "success");
                } else {
                    fnSend({type: 'pollAnswer', respuesta: false, usuario: user, idEncuesta: idEncuesta});
                    swal("Gracias!", "Tu respuesta fue: No", "success");
                }
            }
        );
    });

}

function append(poll){
    let str = `<li><a class="swal-encuesta" href="javascript:;" data-id="${poll.id}"
                data-pregunta="${poll.pregunta}">${truncate.apply(poll.pregunta,[20, true])}</a></li>`;
    $('#panel-encuestas ul div').after(str)
}

export function truncate( n, useWordBoundary ){
    if (this.length <= n) { return this; }
    let subString = this.substr(0, n-1);
    return (useWordBoundary
        ? subString.substr(0, subString.lastIndexOf(' '))
        : subString) + "...";
}

export function renderPoll(pollID, results) {
    let yesPercent = Math.round(100 * (results[1] / (results[0] + results[1]))) ;

    $(`.poll[data-poll=${pollID}] .poll-content`).empty();
    $(`.poll[data-poll=${pollID}] .poll-content`).append(`
        <div class="row m-t-sm m-b-sm">
                <div class="col-lg-6">
                    <h3 class="no-margins font-extra-bold text-success">SI</h3>
                    <div class="font-bold">${results[1]}<i class="fa fa-level-up text-success"></i></div>
                </div>
                <div class="col-lg-6">
                    <h3 class="no-margins font-extra-bold text-success">NO</h3>
                    <div class="font-bold">${results[0]}<i class="fa fa-level-up text-success"></i></div>
                </div>
            </div>
            <div class="progress m-t-xs full progress-small">
                <div style="width: ${yesPercent}%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="25" role="progressbar" class=" progress-bar progress-bar-success">
                    <span class="sr-only">35% Complete (success)</span>
                </div>
            </div>
    `);
}

export function renderRespuestas(respuestas){
    if(respuestas && respuestas.length){
        $('#consultas-chart').empty();
        let respuestasChart = new Chartist.Bar('#consultas-chart', {
            labels: respuestas.map((r) => truncate.apply(r.pregunta, [50, true])),
            series: [
                respuestas.map((r) => r.total - r.si),
                respuestas.map((r) => r.si)
            ]
        }, {
            seriesBarDistance: 5,
            reverseData: true,
            horizontalBars: true,
            axisY: {
                offset: 70
            }
        });

        $('#btn-open-resultados').on('click', () => setTimeout(() => respuestasChart.update(), 200));

        $('body').on('click', '#btn-open-resultados-mobile', () => setTimeout(() => respuestasChart.update(), 200));

    }else{
        $('#consultas-chart').html('No hay respuestas a consultas')
    }
}