let _ = require('lodash');

export function renderGeneral(chats) {
    _.forIn(chats, (chat) => {
        append(chat, '#main-chat');
    })
}

export function renderIndividual(chats){
    _.forIn(chats, (chat) => {
        append(chat, '#private-chat');
    })
}

export function append(chat, selector) {
    let str =
        `<div class="chat-message">\
                <div class="message">\
                    <div class="message-author" href="#">${chat.usuario}:</div>\
                        <span class="message-content">${chat.mensaje}</span>\
                    </div>\
                </div>\
            </div>`;

    $(selector).append(str);
    if(selector === '#private-chat'){
        $('#secondary-chat .panel-body').scrollTop($('#secondary-chat .panel-body')[0].scrollHeight)
    }else{
        $('#primary-chat .panel-body').scrollTop($('#primary-chat .panel-body')[0].scrollHeight)
    }

}


