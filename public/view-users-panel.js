var _ = require('lodash');

export function render(state, usuarioConectado, addStream) {
    $('#RXUsers').empty();

    let pos = 1;
    _.forIn(state.usuarios, (usuario, key) => {
        if(usuario !== null){

            let iconPidePalabra = (usuario.pide_palabra && usuarioConectado.id === state.sesion.usuario_moderador.id )
                ? '<span class="label label-warning pull-right">Pide la Palabra</span>'
                : '';

            let status1 = (state.sesion.usuario_moderador.id === usuario.id) ? "<span class=\"pull-right label label-info\">Tutor</span>" : "";
            let status2 = (state.sesion.usuario_activo.id === usuario.id) ? "<span class=\"pull-right label label-success\">Tiene la palabra</span>" : "";

            let iconPedirPalabra = '';
            let yaTengoLaPalabra =  usuarioConectado.id === state.sesion.usuario_activo.id;
            if(state.sesion.usuario_moderador.id === usuario.id && usuario.id !== usuarioConectado.id && !yaTengoLaPalabra)
                iconPedirPalabra = "<button class=\"btn btn-xs btn-default toggle-request-control\" title=\"Pedir palabra\"><i class=\"pe-7s-speaker\" aria-hidden=\"true\"></i></button>"
            if(state.sesion.usuario_moderador.id === usuario.id && usuario.id !== usuarioConectado.id && yaTengoLaPalabra)
                iconPedirPalabra = "<button class=\"btn btn-xs btn-default toggle-request-control\" title=\"Devolver palabra\"><i class=\"fa fa-microphone-slash\" aria-hidden=\"true\"></i></button>"


            let adminActions = '';
            if(usuarioConectado.id === state.sesion.usuario_moderador.id && usuario.id !== usuarioConectado.id){
                let controlText = (usuario.id === state.sesion.usuario_activo.id) ? 'Recuperar Control' : 'Dar Control';
                let controlIcon = (usuario.id === state.sesion.usuario_activo.id) ? '<i class="fa fa-microphone-slash"></i>' : '<i class="fa fa-microphone"></i>';
                adminActions = `
                    <button class="btn btn-xs btn-default btn-expulsar" title="Bloquear Participacion"><i class="fa fa-user-times"></i></button>
                    <button class="btn btn-xs btn-default btn-dar-control" title="${controlText}">${controlIcon}</button>
                `;
            }

            let localVideo = (usuario.id === usuarioConectado.id) ? 'localVideo' : '';

            let str = `<div class="chat-user" data-userindex="${key}" data-username="${usuario.nombre_usuario}">
                
                <video src="" id="${localVideo}"></video>
                <div class="labels">${status1}${status2}${iconPidePalabra}</div>
                <div class="chat-user-name">
                    <a href="javascript:;">${usuario.nombre_completo}</a>
                </div>
                <div class="btn-group">
                    ${iconPedirPalabra}
                    ${adminActions}
                </div>
            </div>`;

            if(usuario.id === usuarioConectado.id){
                $('#RXUsers').prepend(str);
            }else{
                $('#RXUsers').append(str);
            }

            addStream(usuario.nombre_usuario);

        }

    });
}

