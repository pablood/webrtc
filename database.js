import config from './config';
import mysql from 'mysql';

let DB = mysql.createConnection({
    host: config.db.host,
    user: config.db.usuario,
    password: config.db.password,
    database: config.db.nombre,
    multipleStatements: true
});

DB.connect((err) => {
    if (err) {
        console.error('No se pudo conectar a la base de datos');
        process.exit(1);
    }
});

module.exports = DB;