import { Server as WebSocketServer } from 'ws';
import DB from './database';

let fs = require('fs');
let config =  require('./config');

let https = require('https');
let express = require('express');
let app = express();
let bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('public'));


// Login desde IDEAS
app.post('/login', function(req, res){
    DB.query({
        sql: 'insert into tokens (id_externo, rol, token, nombre_usuario, nombre_completo, curso) values(?, ?, ?, ?, ?, ?)',
        values: [req.body.id, req.body.rol, req.body.token, req.body.nombre_usuario, req.body.nombre_completo, req.body.curso]
    }, (error, results) => {
        if(error) console.log(error);
        res.redirect(`/?token=${req.body.token}`);
    });

});
let wss = null;
let credentials = {};

if(config.ssl){
    let certificate  = fs.readFileSync(config.ssl.certificado);
    let privateKey  = fs.readFileSync(config.ssl.llave);
    credentials = {key: privateKey, cert: certificate};
    let server = https.createServer(credentials, app).listen(config.puerto);

    let WebSocketServer = require('ws').Server;
    wss = new WebSocketServer({
        server: server
    });
}else{
    let server = app.listen(config.puerto, function () {
        let host = server.address().address;
        let port = server.address().port;

        console.log("Example app listening at http://%s:%s", host, port)
    });
    wss =  new WebSocketServer({port: 9090});
}


import InfoMeeting from './infomeeting';
import Sesion from "./sesion";
let infoM = new InfoMeeting();
console.log('Infomeeting running');

app.get('/sesiones', (req, res) =>{

    InfoMeeting.GetSesiones()
        .then(rows => res.send(rows));

});

app.get('/sesiones/:sesionId', (req, res) =>{



    infoM.GetSesion(req.params.sesionId)
        .then(data => res.send(data))
        .catch(error => res.send(error))

});

app.delete('/sesiones/:sesionId', (req, res) =>{

    Sesion.Delete(req.params.sesionId)
        .then ((data) => res.sendStatus(200))
        .catch((err) => res.status(500).send(err))
});



wss.on('connection', function(connection){

    connection.on('message', function(message){
        let data;
        try{
            data = JSON.parse(message);
        }
        catch(e){
            console.log("Invalid JSON");
            data = {}
        }

        switch(data.type){
            // signaling
            case "login": infoM.handleLogin(connection, data); break;
            case "offer": infoM.handleOffer(connection, data); break;
            case "answer": infoM.handleAnswer(connection, data); break;
            case "candidate": infoM.handleCandidate(connection, data); break;

            // interaction
            case "editor": infoM.handleEditor(connection, data); break;
            case "changeActiveUser": infoM.handleChangeActiveUser(connection, data); break;
            case "requestControl": infoM.handleRequestControl(connection, data); break;
            case "changeMode": infoM.handleChangeMode(connection, data); break;
            case "expulsarUsuario": infoM.handleExpulsarUsuario(connection, data); break;

            // chat
            case "chat": infoM.handleChat(connection, data); break;
            case "individualChat": infoM.handleIndividualChat(connection, data); break;

            // polls
            case "createPoll": infoM.handleCreatePoll(connection, data); break;
            case "pollAnswer": infoM.handlePollAnswer(connection, data); break;

            default: InfoMeeting.SendTo(connection, {
                type: "error",
                message: "Command not found: " + data.type
            }); break;
        }
    });

    connection.on('close', function(){
        infoM.closeConnection(connection);
    });

});

