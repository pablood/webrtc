let config = {};

config.webSocketUrl = 'ws://127.0.0.1:9090';
config.puerto = 'numero de puerto';
config.modo = 'poster o video';

config.db = {};
config.db.host = 'host de la base de datos';
config.db.usuario = 'usuario para acceder a de la base de datos';
config.db.password = 'password para acceder de la base de datos';
config.db.nombre = 'nombre de la base de datos';

config.ssl = {};
config.ssl.certificado = 'ruta del certificado';
config.ssl.llave = 'ruta de la llave privada del certificado';

module.exports = config;


